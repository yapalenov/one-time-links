import { Module } from '@nestjs/common';
import { CommonModule } from './common';
import { LinkModule } from './link';

@Module({
  imports: [CommonModule, LinkModule],
})
export class AppModule {}
