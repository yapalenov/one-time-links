import { Request } from 'express';
import * as path from 'path';
import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Param,
  ParseUUIDPipe,
  Post,
  Req,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { LINK_SERVICE } from '../di.constants';
import { ILinkService } from '../interfaces';
import {
  CreateLinkDto,
  CreateLinkResponseDto,
  UseLinkResponseDto,
} from '../dto';

@ApiTags('Links')
@Controller('/')
export class LinkController {
  constructor(
    @Inject(LINK_SERVICE) private readonly linkService: ILinkService,
  ) {}

  @ApiResponse({
    type: CreateLinkResponseDto,
    status: HttpStatus.CREATED,
  })
  @Post()
  public async createLink(
    @Body() body: CreateLinkDto,
    @Req() request: Request,
  ): Promise<CreateLinkResponseDto> {
    const link = await this.linkService.createLink(body.value);

    const urlBase = `${request.protocol}://${request.headers['host']}`;

    const url = new URL(
      path.join(request.originalUrl, link.id),
      urlBase,
    ).toString();

    return { url };
  }

  @ApiResponse({
    type: UseLinkResponseDto,
    status: HttpStatus.OK,
  })
  @Get(':linkId')
  public async useLink(
    @Param('linkId', new ParseUUIDPipe())
    linkId: string,
  ): Promise<UseLinkResponseDto> {
    const { value } = await this.linkService.useLink(linkId);

    return { value };
  }
}
