import { Test, TestingModule } from '@nestjs/testing';
import { Request } from 'express';
import { LinkController } from './link.controller';
import { LINK_SERVICE } from '../di.constants';
import { ILinkService } from '../interfaces';
import {
  CreateLinkDto,
  CreateLinkResponseDto,
  UseLinkResponseDto,
} from '../dto';

describe('LinkController', () => {
  let linkController: LinkController;
  let linkService: ILinkService;
  const linkId: string = 'c5b5657f-7398-4f3c-9551-e45ce0b6090d';
  const linkValue: string = 'super secret info';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LinkController],
      providers: [
        {
          provide: LINK_SERVICE,
          useValue: {
            createLink: jest.fn().mockResolvedValue({
              id: linkId,
            }),
            useLink: jest.fn().mockResolvedValue({ value: linkValue }),
          },
        },
      ],
    }).compile();

    linkController = module.get<LinkController>(LinkController);
    linkService = module.get<ILinkService>(LINK_SERVICE);
  });

  describe('createLink', () => {
    it('should create a link and return the full URL', async () => {
      const body: CreateLinkDto = { value: 'test-value' };
      const request = {
        protocol: 'http',
        headers: { host: 'localhost' },
        originalUrl: '/',
      } as Request;

      const result: CreateLinkResponseDto = await linkController.createLink(
        body,
        request,
      );

      expect(result.url).toBe(`http://localhost/${linkId}`);
      expect(linkService.createLink).toHaveBeenCalledWith(body.value);
    });
  });

  describe('useLink', () => {
    it('should return the value associated with the link', async () => {
      const result: UseLinkResponseDto = await linkController.useLink(linkId);

      expect(result.value).toBe(linkValue);
      expect(linkService.useLink).toHaveBeenCalledWith(linkId);
    });
  });
});
