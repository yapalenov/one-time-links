import { Module } from '@nestjs/common';
import { DatabaseModule } from '../common/database';
import { LinkService } from './services';
import { LINK_SERVICE } from './di.constants';
import { LinkController } from './controllers';

@Module({
  imports: [DatabaseModule],
  controllers: [LinkController],
  providers: [{ provide: LINK_SERVICE, useClass: LinkService }],
})
export class LinkModule {}
