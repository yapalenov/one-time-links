import { ApiProperty } from '@nestjs/swagger';

export class UseLinkResponseDto {
  @ApiProperty()
  value: string;
}
