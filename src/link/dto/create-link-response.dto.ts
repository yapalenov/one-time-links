import { ApiProperty } from '@nestjs/swagger';

export class CreateLinkResponseDto {
  @ApiProperty()
  url: string;
}
