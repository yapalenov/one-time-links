export class LinkIsNotActiveError extends Error {
  constructor(message = 'Ссылка недоступна') {
    super(message);
  }
}
