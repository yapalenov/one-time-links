export class LinkNotFoundError extends Error {
  constructor(message = 'Ссылка не найдена') {
    super(message);
  }
}
