import { Link } from '../entities';

export interface ILinkService {
  createLink(value: string): Promise<Link>;
  useLink(linkId: string): Promise<Link>;
}
