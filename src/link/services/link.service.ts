import { Inject, Injectable } from '@nestjs/common';
import { ILinkService } from '../interfaces';
import { Link } from '../entities';
import { DataSource, Repository } from 'typeorm';
import { DATA_SOURCE } from '../../common/database';
import { LinkIsNotActiveError, LinkNotFoundError } from '../errors';

@Injectable()
export class LinkService implements ILinkService {
  private readonly linkRepository: Repository<Link>;

  constructor(@Inject(DATA_SOURCE) private readonly dataSource: DataSource) {
    this.linkRepository = dataSource.getRepository(Link);
  }

  public async createLink(value: string): Promise<Link> {
    const link = this.linkRepository.create({ value });

    return this.linkRepository.save(link);
  }

  public async useLink(linkId: string): Promise<Link> {
    return this.dataSource.transaction(async (entityManager) => {
      const link = await entityManager.findOne(Link, {
        where: { id: linkId },
        lock: { mode: 'pessimistic_write' },
      });

      if (!link) {
        throw new LinkNotFoundError();
      }

      if (!link.isActive) {
        throw new LinkIsNotActiveError();
      }

      link.isActive = false;

      await entityManager.save(Link, link);

      return link;
    });
  }
}
