import { Module, Provider } from '@nestjs/common';
import { DATA_SOURCE } from './di.constants';
import { dataSource } from './data-source';

export const providers: Array<Provider> = [
  {
    provide: DATA_SOURCE,
    useFactory: async () => dataSource.initialize(),
  },
];

@Module({
  providers: providers,
  exports: providers,
})
export class DatabaseModule {}
