import { DataSource, DataSourceOptions } from 'typeorm';
import { applicationConfiguration } from '../configuration';

export const dataSourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: applicationConfiguration.database.host,
  port: applicationConfiguration.database.port,
  username: applicationConfiguration.database.user,
  password: applicationConfiguration.database.password,
  database: applicationConfiguration.database.databaseName,
  entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/../../../migrations/*{.ts,.js}'],
};

export const dataSource = new DataSource(dataSourceOptions);
