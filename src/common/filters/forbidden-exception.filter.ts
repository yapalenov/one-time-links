import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { LinkIsNotActiveError } from '../../link/errors';

@Catch(LinkIsNotActiveError)
export class ForbiddenExceptionFilter implements ExceptionFilter {
  catch(exception: LinkIsNotActiveError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = HttpStatus.FORBIDDEN;

    response.status(status).json({
      statusCode: status,
      message: exception.message,
    });
  }
}
