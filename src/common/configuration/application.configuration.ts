import * as process from 'process';
import { config } from 'dotenv';
import {
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
  validateSync,
} from 'class-validator';
import { Type } from 'class-transformer';
import { Logger } from '@nestjs/common';
import { DatabaseConfiguration } from './database.configuration';

config();

class ApplicationConfiguration {
  @IsString()
  @IsNotEmpty()
  applicationName: string = process.env.APP_NAME ?? '';

  @IsNumber()
  @IsNotEmpty()
  port = Number(process.env.APP_PORT);

  @Type(() => DatabaseConfiguration)
  @ValidateNested()
  database = new DatabaseConfiguration();
}

export const applicationConfiguration = new ApplicationConfiguration();

const logger = new Logger(ApplicationConfiguration.name);

const validationErrors = validateSync(applicationConfiguration);

if (validationErrors.length > 0) {
  const errorMessages = Array.from(validationErrors).flatMap((error) => {
    return Object.values(error?.children?.[0]?.constraints ?? {}).map(
      (message) => message,
    );
  });

  for (const message of errorMessages) {
    logger.error(message);
    logger.error(new Array(message.length).fill('-').join(''));
  }
  process.exit(1);
}
