import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class DatabaseConfiguration {
  @IsString()
  @IsNotEmpty()
  host: string = process.env.POSTGRES_HOST ?? '';

  @IsNumber()
  @IsNotEmpty()
  port = Number(process.env.POSTGRES_PORT);

  @IsString()
  @IsNotEmpty()
  databaseName: string = process.env.POSTGRES_DB ?? '';

  @IsString()
  @IsNotEmpty()
  user: string = process.env.POSTGRES_USER ?? '';

  @IsString()
  @IsNotEmpty()
  password: string = process.env.POSTGRES_PASSWORD ?? '';
}
