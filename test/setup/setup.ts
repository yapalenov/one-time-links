import { PostgreSqlContainer } from '@testcontainers/postgresql';
import { applicationConfiguration } from '../../src/common/configuration';

module.exports = async () => {
  const container = await new PostgreSqlContainer('postgres:16-alpine')
    .withUsername(applicationConfiguration.database.user)
    .withPassword(applicationConfiguration.database.password)
    .withDatabase(applicationConfiguration.database.databaseName)
    .withExposedPorts(5432)
    .start();

  global.pgContainer = container;

  process.env.POSTGRES_PORT = String(container.getMappedPort(5432));
  process.env.POSTGRES_HOST = container.getHost();
};
