import { dataSourceOptions } from '../../src/common/database/data-source';
import { DataSource } from 'typeorm';

module.exports = async () => {
  const dataSource = new DataSource(dataSourceOptions);

  await dataSource.initialize();
  await dataSource.runMigrations();
};
