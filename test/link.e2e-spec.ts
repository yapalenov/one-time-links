import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import * as request from 'supertest';
import { CreateLinkDto } from '../src/link/dto';
import { isUUID } from 'class-validator';

describe('LinkController (e2e)', () => {
  let app: INestApplication;
  const linkValue: string = 'test-value';
  let createdLinkId: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('400: create link with incorrect body', async () => {
    const createLinkDto = { foo: 'bar' };
    await request(app.getHttpServer())
      .post('/')
      .send(createLinkDto)
      .expect(400);
  });

  it('201: create link', async () => {
    const createLinkDto: CreateLinkDto = { value: linkValue };
    const response = await request(app.getHttpServer())
      .post('/')
      .send(createLinkDto)
      .expect(201);

    createdLinkId = response.body.url.split('/').pop();

    expect(isUUID(createdLinkId)).toBe(true);
  });

  it('400: use a link with incorrect id', async () => {
    await request(app.getHttpServer()).get(`/${createdLinkId}123`).expect(400);
  });

  it('404: use a link that does not exist', async () => {
    await request(app.getHttpServer())
      .get(`/d9a4fcff-eabf-448d-bd5a-9f602de55e6a`)
      .expect(404);
  });

  it('200: use created link', async () => {
    const response = await request(app.getHttpServer())
      .get(`/${createdLinkId}`)
      .expect(200);

    expect(response.body.value).toEqual(linkValue);
  });

  it('403: use created link again', async () => {
    await request(app.getHttpServer()).get(`/${createdLinkId}`).expect(403);
  });
});
