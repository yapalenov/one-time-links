## Установка

```bash
$ npm install
$ cp .env.example .env
```

## Запуск приложения

```bash
$ docker compose up -d
```

## Open API доступен по адресу

```
http://localhost:3000/docs
```

## Тесты

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e
```